int led = 13;
int digital = 10;

void setup()
{
  pinMode(led, OUTPUT);
  pinMode(digital, INPUT);
  digitalWrite(digital, 0);
}

void loop()
{
  int leitura = digitalRead(digital);
  
  if ( leitura == 0 ) {
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }
}
