int buzzer = 9;

//Notas
int DO = 534;
int RE = 588;
int MI = 660;
int FA = 699;
int SOL = 785;
int LA = 881;
int SI = 989;

// Diviso tempo
int base = 400;
int breve = base * 4;
int minima = base * 2;
int seminima = base;
int colcheia = base / 2;
int semicolcheia = base / 4;


void setup()
{
  pinMode(buzzer, OUTPUT);
}

void loop()
{
  /*
  for ( int i=0; i<7; i++ ) {
    tone(buzzer, notes[i], 1000);
    delay(1000);
  }
  */
  
  play(buzzer, MI, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, FA, seminima);
  play(buzzer, SOL, seminima);
  play(buzzer, SOL, seminima);
  play(buzzer, FA, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, RE, seminima);
  play(buzzer, DO, seminima);
  play(buzzer, DO, seminima);
  play(buzzer, RE, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, MI, seminima+colcheia);
  play(buzzer, RE, colcheia);
  play(buzzer, RE , minima);
     
  delay(minima);
  
  play(buzzer, MI, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, FA, seminima);
  play(buzzer, SOL, seminima);
  play(buzzer, SOL, seminima);
  play(buzzer, FA, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, RE, seminima);
  play(buzzer, DO, seminima);
  play(buzzer, DO, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, MI, seminima);
  play(buzzer, RE, seminima+colcheia);
  play(buzzer, DO, colcheia);
  play(buzzer, DO , minima);

  delay(minima);  
  
  play(buzzer, RE , seminima);
  play(buzzer, RE, seminima);
  play(buzzer, MI , seminima);
  play(buzzer, DO , seminima);
  play(buzzer, RE , seminima);
  play(buzzer, MI , colcheia);
  play(buzzer, FA , colcheia);
  play(buzzer, MI , seminima);
  play(buzzer, DO , seminima);
  play(buzzer, RE , seminima);
  play(buzzer, MI , colcheia);
  play(buzzer, FA , colcheia);
  play(buzzer, MI , seminima);
  play(buzzer, DO , seminima);
  play(buzzer, RE , seminima);
  play(buzzer, MI , seminima);
  play(buzzer, DO , minima);
}


void play(int buzzer, int nota, int tempo)
{
  tone(buzzer, nota, 1000);
  delay(tempo);
  delay(10);
}
