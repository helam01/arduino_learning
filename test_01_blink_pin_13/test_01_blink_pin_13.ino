int led = 13;
int baseTime = 400;

void setup()
{
  pinMode(led, OUTPUT);
}


void loop()
{
  for( int x=0; x <3; x++ ) {
    playSeminima();
  }
  
  for( int x=0; x <2; x++ ) {
    playColcheia();
  }
  
  playSeminima();
  
  for( int x=0; x <2; x++ ) {
    playColcheia();
  }
  
  playSeminima();
  
}


void playSeminima()
{
  digitalWrite(led, HIGH);
  delay(baseTime);
  digitalWrite(led, LOW);
  delay(baseTime);
}

void playColcheia()
{
  int colcheia = baseTime / 2;
  
  digitalWrite(led, HIGH);
  delay(colcheia);  
  digitalWrite(led, LOW);
  delay(colcheia);
}

void playSemiColcheia()
{
  int colcheia = baseTime / 4;
  
  digitalWrite(led, HIGH);
  delay(colcheia);  
  digitalWrite(led, LOW);
  delay(colcheia);
}
