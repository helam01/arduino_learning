int sensor = 5;
int LED = 11;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
   int valorSensor = analogRead(sensor);
   Serial.println(valorSensor);
   
   int valorPWM = map( valorSensor, 100, 800, 155, 0 );
   analogWrite(LED, valorPWM);
}
